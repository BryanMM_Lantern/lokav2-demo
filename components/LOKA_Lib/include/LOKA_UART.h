/**
 * @file UART.h
 * @author Andres Arias
 * @date 13/3/2019
 * @brief Simple UART driver.
 */

#ifndef UART_H
#define UART_H

#include <string>
#include <cstring>
#include <stdio.h>
#include "driver/uart.h"
#include "esp_log.h"

#define UART_TAG            "UART"
#define WISOL_RX_FLOW_CTRL  122
#define UART_TIMEOUT        100

namespace drivers
{
    /**
     * @class UART
     * @brief Low-level driver for the UART bus.
     */
    class UART {

        public:

            /**
             * @brief Initializes a UART port with the given parameters.
             * @param[in] port_num ESP32 UART number. You can use the provided 
             * macros UART_NUM_0, UART_NUM_1, UART_NUM_2.
             * @param[in] tx_pin GPIO to use as TX.
             * @param[in] rx_pin GPIO to use as RX.
             * @param[in] rts_pin GPIO to use as RTS (optional).
             * @param[in] CTS_pin GPIO to use as CTS (optional).
             * @param[in] baud_rate Baud rate to use (default 9600).
             * @param[in] buffer_size Size in bytes for the reception
             * buffer, where read data will be initially stored.
             * @param[in] word_length Size in bits for each word (default UART_DATA_8_BITS).
             * @param[in] parity Enables parity check for consistency. Can check if odd or even
             * (default UART_PARITY_DISABLE).
             * @param[in] stop_bits UART stop bits (default UART_STOP_BITS_1).
             * @param[in] flow_control Enables hardware flow control (default UART_HW_FLOWCTRL_DISABLE).
             */
            UART(uart_port_t port_num,
                    int tx_pin,
                    int rx_pin,
                    int rts_pin = UART_PIN_NO_CHANGE,
                    int cts_pin = UART_PIN_NO_CHANGE,
                    int baud_rate = 9600,
                    int buffer_size = 256,
                    uart_word_length_t word_length = UART_DATA_8_BITS,
                    uart_parity_t parity = UART_PARITY_DISABLE,
                    uart_stop_bits_t stop_bits = UART_STOP_BITS_1,
                    uart_hw_flowcontrol_t flow_control = UART_HW_FLOWCTRL_DISABLE,
                    uint8_t rx_flow_ctrl_thresh = WISOL_RX_FLOW_CTRL
                );

            /**
             * @brief Disables the UART driver once it's not required.
             */
            ~UART();

            /**
             * @brief Used for error checking, will report any error with
             * the driver initialization, or ESP_OK if the driver initialized
             * successfully. 
             * @return Channel status, refer to ESP32 error codes.
             */
            esp_err_t get_status();

            /**
             * @brief Writes a string on the UART channel.
             * @param[in] String with the data to be written.
             * @param[in] with_break Enable if you want to add a delay after transmission.
             * @param[in] break_len If with_break is enabled, the amout of ticks to wait.
             * @return Amount of bytes written or -1 if there was an error.
             */
            int write(const std::string &data,
                    bool with_break = false,
                    int break_len = 0);

            /**
             * @brief Reads data from the RX buffer.
             * @param[in] timeout Amount of ticks to wait before calling a timeout.
             * @return The characters read from the RX buffer.
             */
            std::string read(unsigned int timeout = 0);

            /**
             * @brief Discards data from the RX buffer.
             */
            void flush();

        private:
            uart_port_t port_num;
            TaskHandle_t uart_task_handler;
            int buffer_size;
            esp_err_t status;
    };

} /* drivers */ 

#endif
