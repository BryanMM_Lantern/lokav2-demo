//**********************************************************************************************************************************
// Filename: BOARD_LokaV2.cpp
// Date: 18.10.2017
// Author: Rafael Pires
// Company: LOKA SYSTEMS
// Version: 1.0
// Description: Code to operate the LOKA V2 board firmware
//**********************************************************************************************************************************
#ifndef BOARD_LOKAV2_ULP_H_
#define BOARD_LOKAV2_ULP_H_


//**********************************************************************************************************************************
//                                                      Includes Section
//**********************************************************************************************************************************
#include "BoardIO.h"

#include "esp32/ulp.h"
#include "soc/sens_reg.h"
#include "string.h"
#include "esp_task_wdt.h"

#include "soc/rtc.h"
#include "soc/rtc_cntl_reg.h"
#include "soc/timer_group_reg.h"

#include "soc/apb_ctrl_reg.h"


//**********************************************************************************************************************************
//                                                      Defined Section
//**********************************************************************************************************************************
#define ULP_TIMER_WAKEUP			0x01
#define ULP_BUTTON_WAKEUP			0x02
#define ULP_ACCELEROMETER_WAKEUP	0x04
#define ULP_INTERRUPT_WAKEUP		0x08


//**********************************************************************************************************************************
//                                                      Templates Section
//**********************************************************************************************************************************
class BoardULP{
public:
	static void init();
	static uint64_t getWakeUpTime(unsigned int seconds);

	static void setWakeUpTime(unsigned int seconds);
	static void lockBattery();
	static void enableWakeUp(unsigned int mask);
	static void disableWakeUp(unsigned int mask);
	static uint64_t getCurrentTime();
	static void resetWakeUp();
	static void setWakeUp(unsigned int mask);
	static void printDebug();
private:
};


#endif
