/*
	LOKA V2 DEMO EXAMPLE

	Simple message sending program using sigfox protocol.

	This example code is in the Public Domain (or CC0 licensed, at your option.)

	Unless required by applicable law or agreed to in writing, this
	software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
	CONDITIONS OF ANY KIND, either express or implied
*/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"	    // included for vTaskDelay function

#include "BOARD_LokaV2.h"		// included for sleep function
#include "BOARD_LokaV2_ULP.h"	

#include "LOKA_PRIMIS_WiSOL.h"  // included for turnOn, turnOff and sendData functions for Sigfox

#define MAIN_TAG "Main"

extern "C"
{
	void app_main(void);
}

void app_main()
{

	ESP_LOGI(MAIN_TAG, "LOKA V2 Demo");

	// Create a random message
    char message[12] = {0x1,0x2,0x3,0x4,0x5,0x6,
                        0x7,0x8,0x9,0xa,0xb,0xc};
	std::string sigFoxPayload(message,12);
    
	// Instance of the Sigfox driver
	drivers::WiSOL sigfoxDriver;

	ESP_LOGI(MAIN_TAG,"Turning Sigfox Radio On");
	sigfoxDriver.turnOn();
	
	ESP_LOGI(MAIN_TAG,"Sending Sigfox Uplink Message");
	sigfoxDriver.sendData(sigFoxPayload);

	ESP_LOGI(MAIN_TAG,"Turning Sigfox Radio Off");
	sigfoxDriver.turnOff();

	// Pause the execution for 3 seconds
	vTaskDelay(3000 / portTICK_PERIOD_MS);

	unsigned int sleepSeconds = 60; 

	// Go to deep sleep mode
	ESP_LOGI(MAIN_TAG, "Going to sleep now");
	BoardULP::init();
    Board::gotoLowPowerMode(sleepSeconds,NULL);
}
