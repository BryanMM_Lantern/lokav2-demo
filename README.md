# LOKA V2 DEMO 

This is an example project for developing applications using Loka v2 based on ESP-IDF v3.3.

## Getting Started

Following this tutorial you will be able to create your own aplications that include Loka v2 libraries for using Sigfox protocol, deepsleep mode, UART. You can find a complete documentation in https://lokav2-sdk.lantern.rocks/.
### Prerequisites

* Install and configure ESP-IDF >= v3.3 following the steps described in https://docs.espressif.com/projects/esp-idf/en/latest/get-started/index.html.

* Clone this example project repository https://gitlab.com/lantern-public/lokav2-demo.git where you want. 


### Compile and run the Project 

1. From terminal, change directory to cloned example project folder and then run **make menuconfig**. The console will show a menu where you need to change following configurations: 

* In serial flasher: ensure to select the correct serial port used by device when is connected (Usually /dev/ttyUSB0).

2. Run **make -j8** to compile the project and ESP-IDF components.

3. Connect the Neuron device and run **./update_info.sh** to write necessary partitions information on Neuron flash memory **Ensure that the port selected in ./update_info.sh file matches with device selected (Usually /dev/ttyUSB0)s. 

4. Then run **make flash** to flash the compiled program into the Neuron

5. Finally run **make monitor** to see the output logs of the flashed program. 

6. If you detect weird behavior in flash memory you can run **make erase_flash** for delete the content of flash memory.


## Examples 

### Sigfox Uplink

Send an uplink message over Sigfox network. 

```
#include "LOKA_PRIMIS_WiSOL.h"       // Header for Sigfox driver
 
std::string data = "test"            // Define the data to send

drivers::WiSOL sigfox_driver;        // Instanciate Sigfox S2LP
sigfox_driver.turnOn();              // Turning Sigfox Radio On
sigfox_driver.sendData(data);        // Send data over Sigfox Network
sigfox_driver.turnOff();             // Turning off Sigfox Radio

```

### Sigfox Downlink

Send an downlink message over Sigfox network.

```
#include "LOKA_PRIMIS_WiSOL.h"                                               // Header for the Sigfox driver

std::string data = "test";                                                   // Generate the desired downlink message

drivers::WiSOL sigfox_driver;                                                // Instanciate Sigfox S2LP
sigfox_driver.turnOn();                                                      // Turning Sigfox Radio On
std::string downlink_response = sigfox_driver.sendAndReadData(data);         // Send data over Sigfox Network
sigfox_driver.turnOff();                                                     // Turning off Sigfox Radio

```


### Deep sleep Mode

Put the device in deepsleep mode that for amount of seconds consumming less than 10uA. After deepsleep the device wake up with a reset and run the main application. **This method is not secure for less than 3 seconds.**

```
#include "BOARD_LokaV2.h"                // Header for low power consumption
#include "BOARD_LokaV2_ULP.h"            // Header for deep sleep dependency

unsigned int seconds = 10;               // Amount of seconds that device will be in deepsleep mode
BoardULP::init();                        // Initialize the ULP.
Board::gotoLowPowerMode(seconds,NULL);   // Going to deepsleep

```